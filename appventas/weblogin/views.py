from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import LoginForm, RegistroForm
from .models import Usuario,Persona
# Create your views here.
def login(request):
    #return HttpResponse("estoy en la pagina de login")
    mensaje={'txt_login':"Ingrese Usuario",'estado':""}
    usuariodb=None
    if request.method == 'POST':
        
        form = LoginForm(data=request.POST)

        
        # if Usuario.objects.filter(headline="Test")

        # if form.is_valid():
        usuario = request.POST.get('usuario','')
        usuariodb = Usuario.objects.filter(usuario=usuario)
        if len(usuariodb)>0:
            clave = request.POST.get('clave','')
            clavedb = usuariodb.filter(clave=clave)
            if len(clavedb)>0:
                mensaje["estado"]= "valido"
                return redirect('portal')
        else:
            mensaje["estado"]= "Usuario No valido"

        mensaje['form_login']=form
        return render(request,'login.html',mensaje)
    else:
        form = LoginForm(data=request.GET)
        mensaje['form_login']=form
        return render(request,'login.html',mensaje)

def registro(request):
    return HttpResponse("estoy en la pagina de registro")