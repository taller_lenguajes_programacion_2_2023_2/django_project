from django import forms
from .models import Usuario, Persona


class LoginForm(forms.Form):
    usuario = forms.CharField(required=True,widget=forms.EmailInput(   
    ))
    clave =forms.CharField(required=True,widget=forms.PasswordInput(
        
    ))
    
    class Meta:
        model = Usuario

class RegistroForm(forms.Form):
    nombre = ""
    apellido = ""
    email = ""
    f_nacimiento=""
    # usuario = models.ForeignKey(Usuario,on_delete=models.SET_NULL,null=True)
    
    class Meta:
        model = Persona